package org.ylzl.eden.full.tracing.constant;

import lombok.experimental.UtilityClass;

/**
 * 压测标记
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
@UtilityClass
public class StressTag {

	public static final String STRESS_TAG = "stress";
}
