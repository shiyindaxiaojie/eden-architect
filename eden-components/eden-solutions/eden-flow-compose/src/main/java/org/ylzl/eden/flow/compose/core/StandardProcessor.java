package org.ylzl.eden.flow.compose.core;

/**
 * 标准流程
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
public abstract class StandardProcessor<T> extends AbstractProcessor<T> {
}
