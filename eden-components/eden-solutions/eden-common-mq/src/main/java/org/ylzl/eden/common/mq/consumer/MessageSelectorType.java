package org.ylzl.eden.common.mq.consumer;

import lombok.experimental.UtilityClass;

/**
 * 消息模型
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
@UtilityClass
public class MessageSelectorType {

	public static final String TAG = "TAG";

	public static final String SQL92 = "SQL92";
}
