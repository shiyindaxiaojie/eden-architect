package org.ylzl.eden.common.mq.consumer;

import lombok.experimental.UtilityClass;

/**
 * 消息模型
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
@UtilityClass
public class MessageModel {

	public static final String BROADCASTING = "BROADCASTING";

	public static final String CLUSTERING = "CLUSTERING";
}
