package org.ylzl.eden.extension.context;

/**
 * 组件生命周期适配器
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
public abstract class LifecycleAdapter implements Lifecycle {

	@Override
	public void initialize() throws IllegalStateException {

	}

	@Override
	public void start() throws IllegalStateException {

	}

	@Override
	public void destroy() throws IllegalStateException {

	}
}
