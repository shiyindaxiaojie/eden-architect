package org.ylzl.eden.cola.extension;

/**
 * 业务场景
 *
 * @author <a href="mailto:shiyindaxiaojie@gmail.com">gyl</a>
 * @since 2.4.13
 */
public class BizScenario {

	public final static String DEFAULT_BIZ_ID = "#defaultBizId#";

	public final static String DEFAULT_USE_CASE = "#defaultUseCase#";

	public final static String DEFAULT_SCENARIO = "#defaultScenario#";

	private final static String DOT_SEPARATOR = ".";
}
